﻿/*
 * Erstellt mit SharpDevelop.
 * Benutzer: rwolf
 * Datum: 06.12.2016
 * Zeit: 09:34
 * 	Version 1.1
 * 	    - added error handling		 
 * 	    - added error handling
 * Sie können diese Vorlage unter Extras > Optionen > Codeerstellung > Standardheader ändern.
 * 	Version 3.0
 * 	    - added error handling
 * 	    - added both snap shot modes: SW Trigger and 
 * 	    - new in connect camera
 * 	        1.set default settings at first
 * 	        2.adjust package size by HW 	    
 * 	    - added save default settings function if not been saved
 * 	    - added save settings on connect if request
 * 	    - added save setting on error
 * 	    - added reverse x and y commands
 */
using System;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Collections.Generic;
using System.IO;
using Basler.Pylon;

namespace ConnectBaslerCameras
{
    class ConnectBaslerCameras
    {
        private const string c_DLLPATH = @"C:\Windows\Microsoft.NET\assembly\GAC_64\Basler.Pylon\v4.0_1.0.0.0__e389355f398382ab\Basler.Pylon.dll";
        private const string c_GVSPAdjustPacketSize = "GVSPAdjustPacketSize";
        private const string c_GETGVSPPACKETSIZE = "GevSCPSPacketSize";
        private const string c_GVSPPacketSize = "GVSPPacketSize";
        private const string cc_ADJUSTGVSPPACKETSIZE = "ADJUSTGVSPPACKETSIZE";
        private const string cc_MODEL = "MODEL";
        private const string c_StreamBytesPerSecond = "STREAMBYTESPERSECOND";

        private const string c_OK = "OK";
        private const string c_ERROR = "ERROR";
        private const string c_BASLERAPI_VERSION = "BASLERAPI_VERSION";
        private const string c_SERVICEVERSION = "SERVICEVERSION";
        private const string c_Connected_withIP = "Camera connected with";
        private const string c_CAMCONNECTED = "Camera connected";
        private const string c_DISCONNECTED = "Camera disconnected";

        private const string c_NOID_OR_NOTOPEN = "No camera id given or not opened";
        private const string c_NO_CAMERA = "NOTFOUND: no cameras found";
        private const string c_NO_CAMERA_ID = "No camera id given";
        private const string c_CONNECTING_ERROR = "Error while connecting camera";
        private const string c_UNKNOWCOMMAND = "unknown command";
        private const string c_SETFEATUREVALUES = "SETFEATUREVALUES";
        private const string c_GETFEATUREVALUES = "GETFEATUREVALUES";
        private const string c_EXCEPTION = "EXCEPTION";

        private const string c_ReverseX = "REVERSEX";
        private const string c_ReverseY = "REVERSEY";
        private const string c_Gain = "gain";

        static string[] commands = {
            "HALT",
            "DISCONNECT",
            "CAMERALIST",
            "CONNECTCAM",
            "DISCONNECTCAM",
            "SNAPSHOT",
            "GETFEATUREVALUES",
            "SETFEATUREVALUES",
            "PUSHLASTBYTES",
            "SWVERSION",
            "GETGVSPPACKETSIZE",
            "SAVECAMSETTINGSONERROR",
            "LOADCAMSETTINGS",
            "SAVESERVICELOG"
        };

        static TcpListener Server;
        static bool vServerContinue = true;
        static NetworkStream stream;
        static Dictionary<string, Camera> m_CamsConnected;

        static byte[] lastBuffer;
        static bool lastGrabSucceeded;
        static IGrabResult lastGrabResult;
        static ManualResetEvent evt;

        public static String GetVersionFromFileName(string ProcessName)
        {
            string ResultDev = "";
            try
            {
                if (!File.Exists(ProcessName))
                    return "File not found";
                FileVersionInfo vFileVersionInfo = FileVersionInfo.GetVersionInfo(ProcessName);
                string vVerPrd = vFileVersionInfo.ProductVersion;
                if (vVerPrd == "0.0.0.0")
                {
                    ResultDev = "\n                         Developer Version"
                        + "\n                         LastUpdate: " + GitConstants.LASTUPDATE
                        + "\n                         Based on: "   + GitConstants.GIT_LAST_TAG
                        + "\n                         Short Hash: " + GitConstants.GIT_LAST_HASH
                        + "\n                         Branch: "     + GitConstants.GIT_CURRENT_BRANCH
                        + "\n                         Author: "     + GitConstants.GIT_AUTHOR_NAME
                        + "\n                         Commit: "     + GitConstants.GIT_COMMIT_DATE;
                    return ResultDev;
                }
                else
                    return vVerPrd;
            }
            catch (Exception exception)
            {
                SendERResponse(c_EXCEPTION, "", exception.Message);
            }
            return "Exception: cannot extract version number";
        }

        public static Process PriorProcess()
        // Returns a System.Diagnostics.Process pointing to
        // a pre-existing process with the same name as the
        // current one, if any; or null if the current process
        // is unique.
        {
            try


            {
                Process curr = Process.GetCurrentProcess();
                Process[] procs = Process.GetProcessesByName(curr.ProcessName);
                foreach (Process p in procs)
                {
                    if ((p.Id != curr.Id) &&
                        (p.MainModule.FileName == curr.MainModule.FileName))
                        return p;
                }
            }
            catch (Exception exception)
            {
                SendERResponse(c_EXCEPTION, "", exception.Message);
            }
            return null;
        }

        public static void Main(string[] args)
        {
            string strExeFilePath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            string LastWriteTime = File.GetLastWriteTime(strExeFilePath).ToString(("yyyy.MM.dd hh:mm"));
            Console.WriteLine(AddTimeStamp() + "Starting Basler Camera Service 64 Bit. Last write time: " + LastWriteTime);
            Console.WriteLine(AddTimeStamp() + "Camera Service Version: " + GetVersionFromFileName(System.Reflection.Assembly.GetExecutingAssembly().Location));
            Console.WriteLine("                         Basler .NET API Version: " + GetVersionFromFileName(c_DLLPATH));

            if (PriorProcess() != null)
            {
                Console.WriteLine(AddTimeStamp() + "Another instance of the app is already running.");
                return;
            }

            const Int32 port = 13001;
            IPAddress vLocalAdress = IPAddress.Parse("127.0.0.1");
            bool vContinueOneClient = true;

            Server = new TcpListener(vLocalAdress, port);
            Server.Server.NoDelay = true;
            evt = new ManualResetEvent(false);
            var bytes = new Byte[256];
            string data = null;
            m_CamsConnected = new Dictionary<string, Camera>();

            Server.Start();
            try
            {
                while (vServerContinue)
                {
                    try
                    {
                        Console.WriteLine(AddTimeStamp() + "waiting for connection at port 13001..");
                        TcpClient client = Server.AcceptTcpClient();
                        stream = client.GetStream();
                        vContinueOneClient = true;

                        Console.WriteLine();
                        SendOKResponse(c_Connected_withIP, "", client.Client.LocalEndPoint.ToString() + "  port " + port.ToString());
                        SendOKResponse(c_SERVICEVERSION, "", GetVersionFromFileName(System.Reflection.Assembly.GetExecutingAssembly().Location));
                        SendOKResponse(c_BASLERAPI_VERSION, "", GetVersionFromFileName(c_DLLPATH));
                        Console.WriteLine();

                        int i;
                        // Loop to receive all the data sent by the client.
                        while (vContinueOneClient)
                        {
                            if ((i = stream.Read(bytes, 0, bytes.Length)) == 0)
                                break;
                            // Translate data bytes to a ASCII string.
                            data = Encoding.ASCII.GetString(bytes, 0, i);
                            //            Console.Write(AddTimeStamp() + "Received: {0}", data);

                            // Process the data sent by the client.
                            data = data.Trim().ToUpper();
                            try
                            {
                                vContinueOneClient = CheckCommand(data);
                            }
                            catch (Exception e)
                            {
                                SendERResponse(c_EXCEPTION, "", e.Message);
                            }
                            if (!vContinueOneClient)
                                client.Close();
                        }
                    }
                    catch (Exception e)
                    {
                        SendERResponse(c_EXCEPTION, "", e.Message);
                    }
                } // End While

            }
            finally
            {
                DisconnectAllCams();
                Server.Stop();
            }
        }

        static void DisconnectAllCams()
        {
            Console.WriteLine(AddTimeStamp() + " DisconnectAllCams");
            foreach (Camera cam in m_CamsConnected.Values)
            {
                cam.Close();
            }
            m_CamsConnected.Clear();
        }
        static bool CheckCommand(string message)
        {
            var msgSplit = new List<string>();
            msgSplit.AddRange(message.ToUpper().Split(new char[] { '<', '|', '>' }, StringSplitOptions.RemoveEmptyEntries));
            int idx = Array.IndexOf(commands, msgSplit[0]);
            string toSend = "";
            string CamID = "";
            string vResult;
            try
            {
                switch (idx)
                {
                    case 0: // halt
                        SendOKResponse(commands[idx], "", "");
                        vServerContinue = false;
                        return false;
                    case 1: // disconnect
                        SendOKResponse(commands[idx], "", "");
                        return false;
                    case 2: // cameralist
                        toSend = buildCamList();
                        if (toSend.Length == 0)
                            SendERResponse(commands[idx], "", c_NO_CAMERA);
                        else
                            SendOKResponse(commands[idx], "", toSend);
                        break;
                    case 3: // CONNECTCAM
                        CamID = GetIDFromMessage(message);
                        if (CamID == "")
                        {
                            SendERResponse(commands[idx], CamID, c_NO_CAMERA_ID);
                            break;
                        }
                        bool IsResetSettings = Convert.ToBoolean(GetTextFromMessage(message, 5));
                        if (ConnectCam(CamID, GetTextFromMessage(message, 2), GetTextFromMessage(message, 3), IsResetSettings))
                            SendOKResponse(commands[idx], CamID, c_CAMCONNECTED);
                        else
                            SendERResponse(commands[idx], CamID, c_CONNECTING_ERROR);
                        break;
                    case 4: // DISCONNECTCAM
                        CamID = GetIDFromMessage(message);
                        if (!m_CamsConnected.ContainsKey(CamID))
                        {
                            SendERResponse(commands[idx], CamID, c_NOID_OR_NOTOPEN);
                            break;
                        }
                        DisconnectCam(CamID);
                        break;
                    case 5: // SNAPSHOT
                       // int vTick = Environment.TickCount;
                        CamID = GetIDFromMessage(message);
                        if (!m_CamsConnected.ContainsKey(CamID))
                        {
                            SendERResponse(commands[idx], CamID, c_NOID_OR_NOTOPEN);
                            break;
                        }
                        if (SendCameraSnapshot(CamID))
                            SendOKResponse(commands[idx], CamID, "");
                        //Console.WriteLine(AddTimeStamp() + "SnapShot: " + (Environment.TickCount - vTick).ToString());
                        break;
                    case 6: // GETFEATUREVALUES
                        CamID = GetIDFromMessage(message);
                        if (m_CamsConnected.ContainsKey(CamID))
                            SendOKResponse(commands[idx], CamID, getFeatureList(message));
                        else
                            SendERResponse(commands[idx], CamID, c_NOID_OR_NOTOPEN);
                        break;
                    case 7: // SETFEATUREVALUES
                        CamID = GetIDFromMessage(message);
                        if (m_CamsConnected.ContainsKey(CamID))
                        {
                            vResult = setFeatures(message);
                            if (vResult.Contains(c_EXCEPTION))
                            {
                                SendERResponse(commands[idx], CamID, vResult);
                            }
                            else
                                SendOKResponse(commands[idx], CamID, message);
                        }
                        else
                            SendERResponse(commands[idx], CamID, c_NOID_OR_NOTOPEN);
                        break;
                    case 8: //pushlastbytes
                        break;
                    case 9://SWVERSION
                        SendOKResponse(commands[idx], "", GetVersionFromFileName(System.Reflection.Assembly.GetExecutingAssembly().Location));
                        break;
                    case 10://GETGVSPPACKETSIZE
                        CamID = GetIDFromMessage(message);
                        if (!m_CamsConnected.ContainsKey(CamID))
                        {
                            SendERResponse(commands[idx], CamID, c_NOID_OR_NOTOPEN);
                            break;
                        }
                        if (GetGVSPPacketSize(CamID, out vResult))
                            SendOKResponse(commands[idx], CamID, vResult);
                        else
                            SendERResponse(commands[idx], CamID, "");
                        break;
                    case 11://SAVECAMSETTINGSONERROR
                        CamID = GetIDFromMessage(message);
                        if (!m_CamsConnected.ContainsKey(CamID))
                        {
                            SendERResponse(commands[idx], CamID, c_NOID_OR_NOTOPEN);
                            break;
                        }
                        if (SaveCAMSettingsOnError(m_CamsConnected[CamID], GetTextFromMessage(message, 2)))
                            SendOKResponse(commands[idx], CamID, "");
                        break;
                    case 12://LOADCAMSETTINGS
                        //no function
                        break;
                    case 13://SAVESEVICELOG
                            //SaveServiceLogOnError;
                        break;
                    default:
                        SendERResponse(c_UNKNOWCOMMAND, "", message);
                        break;
                }
            }
            catch (Exception e)
            {
                SendERResponse(c_EXCEPTION, CamID, "1" + e.Message);
            }
            return true;
        }

        static string setFeatures(string msg)  // SETFEATUREVALUES
        {
            string result = "";
            string featureName;
            string value = "";
            string[] split = msg.Split(new[] { '<', '|', '>' }, StringSplitOptions.RemoveEmptyEntries);
            string CamID = split[1];

            result = CamID;
            for (int i = 2; i <= split.Length - 1; i++)
            {
                featureName = split[i].Split(new[] { '=' })[0];
                value = split[i].Split(new[] { '=' })[1];

                if (featureName.ToLower() == "gain")
                {
                    m_CamsConnected[CamID].Parameters[PLCamera.GainRaw].SetValue(Int32.Parse(value));
                    result = result + "|" + featureName + "=" + value;
                }
                else if (featureName.ToLower() == c_ReverseX.ToLower())
                {
                    if (value.ToLower() == "true")
                    {
                        m_CamsConnected[CamID].Parameters[PLCamera.ReverseX].SetValue(true);
                    }
                    else
                    {
                        m_CamsConnected[CamID].Parameters[PLCamera.ReverseX].SetValue(false);
                    }

                    result = result + "|" + featureName + "=" + value;
                }
                else if (featureName.ToLower() == c_ReverseY.ToLower())
                {
                    if (value.ToLower() == "true")
                    {
                        m_CamsConnected[CamID].Parameters[PLCamera.ReverseY].SetValue(true);
                    }
                    else
                    {
                        m_CamsConnected[CamID].Parameters[PLCamera.ReverseY].SetValue(false);
                    }

                    result = result + "|" + featureName + "=" + value;
                }
                else
                {
                    result = result + "|" + featureName + "=" + value.ToString();
                }
            }
            return result;
        }
        static string buildCamList()
        {
            string result = "";
            Console.Write(AddTimeStamp() + "   Cameras found:");
            foreach (ICameraInfo cam in CameraFinder.Enumerate())
            {
                result = result + cam[CameraInfoKey.DeviceMacAddress] + ",";
                Console.WriteLine();
                Console.Write("                            " + cam[CameraInfoKey.DeviceMacAddress]);
            }
            Console.WriteLine();
            return result.TrimEnd(new char[] { ',' });
        }

        static void SendOKResponse(string msg, string ID, string Text)
        {
            msg = c_OK + "|" + msg + "|" + ID + "|" + Text;
            SendString(msg);
            if (!msg.Contains("SNAPSHOT"))
            Console.WriteLine(AddTimeStamp() + msg);
        }
        static void SendERResponse(string msg, string ID, string Text)
        {
            msg = c_ERROR + "|" + msg + "|" + ID + "|" + Text;
            SendString(msg);
            Console.WriteLine(AddTimeStamp() + msg);
        }
        static bool SaveCAMSettingsOnOpen(Camera camera, bool SaveFile, bool SetDefault)
        {
            bool result = false;
            if (SetDefault | SaveFile)
            {
                try
                {
                    camera.Open();
                    if (!camera.IsOpen)
                        return result;

                    if (SaveFile)//UserSet2 as last settings back up
                    {
                        camera.Parameters[PLCamera.UserSetSelector].SetValue(PLCamera.UserSetSelector.UserSet2);
                        camera.Parameters[PLCamera.UserSetSave].Execute();
                        SendOKResponse("Basler: Save last settings as UserSet2", "", "");
                    }
                    if (SetDefault)//  restore factory settings by loading user default set
                    {
                        camera.Parameters[PLCamera.UserSetSelector].SetValue(PLCamera.UserSetSelector.Default);
                        camera.Parameters[PLCamera.UserSetLoad].Execute();
                        SendOKResponse("Basler: restore factory settings", "", "");
                    }
                    //save camera settings into UserSet1 and set it as start up set
                    camera.Parameters[PLCamera.UserSetSelector].SetValue(PLCamera.UserSetSelector.UserSet1);
                    camera.Parameters[PLCamera.UserSetSave].Execute();
                    camera.Parameters[PLCamera.UserSetDefaultSelector].SetValue(PLCamera.UserSetDefaultSelector.UserSet1);
                    SendOKResponse("Basler: Save default settings as UserSet1", "", "");

                    camera.Close();
                    result = true;
                }
                catch (Exception exception)
                {
                    result = false;
                    SendERResponse(c_EXCEPTION, "", exception.Message);
                }
            }
            else
                result = true;
            return result;
        }
        static string getFeatureList(string msg) // GETFEATUREVALUES
        {
            string result = "";
            try
            {
                string[] split = msg.Split(new[] { '<', '|', '>' }, StringSplitOptions.RemoveEmptyEntries);
                string CamID = split[1];
                if (split[2].ToLower().Contains(c_Gain))
                {
                    result = CamID + "|GAIN=" + m_CamsConnected[CamID].Parameters[PLCamera.GainRaw];
                }
                else if (split[2].ToLower().Contains(c_GVSPPacketSize.ToLower()))
                    result = result + "|" + c_GVSPPacketSize + "= " + m_CamsConnected[CamID].Parameters[PLCamera.GevSCPSPacketSize];
            }
            catch (Exception e)
            {
                result = "no value";
                SendERResponse(c_EXCEPTION, "", e.Message);
            }
            return result;
        }
        static void SaveServiceLogOnError()
        {
            FileStream ostrm;
            StreamWriter writer;
            TextWriter oldOut = Console.Out;
            string filePath = @"C:\OurPlant\Logs\CameraSettings\ConsoleOnError\";
            if (!Directory.Exists(filePath))
            {
                DirectoryInfo folder = Directory.CreateDirectory(filePath);
            }
            // DirectoryInfo di = Directory.CreateDirectory(filePath);
            string fileName = "ConsoleOnError" + DateTime.Now.ToString("yyyyddMM_HHmmss") + ".txt";
            try
            {
                ostrm = new FileStream(filePath + fileName, FileMode.OpenOrCreate, FileAccess.Write);
                writer = new StreamWriter(ostrm);
            }
            catch (Exception e)
            {
                Console.WriteLine("Cannot open" + filePath + fileName);
                Console.WriteLine(e.Message);
                return;
            }
            Console.SetOut(writer);
            Console.SetOut(oldOut);
            writer.Close();
            ostrm.Close();
            Console.WriteLine("Done");
        }
        static bool SaveCAMSettingsOnError(Camera camera, string FileName)
        {
            bool result = false;
            if (!camera.IsConnected)
                return result;
            try
            {
                camera.Close();
                camera.Open();
                if (camera.IsOpen)
                {
                    camera.Parameters[PLCamera.UserSetSelector].SetValue(PLCamera.UserSetSelector.UserSet3);
                    camera.Parameters[PLCamera.UserSetSave].Execute();
                    SendOKResponse("Basler: Save settings on error as UserSet3", "", camera.CameraInfo.ToString());

                }
                camera.Close();
                result = true;
            }
            catch (Exception exception)
            {
                result = false;
                SendERResponse(c_EXCEPTION, "", exception.Message);
            }
            return result;
        }
        static bool GetGVSPPacketSize(string ID, out string size)
        {
            bool result = false;
            size = "no value";
            if (!m_CamsConnected[ID].IsOpen)
                return result;
            try
            {
                size = m_CamsConnected[ID].Parameters[c_GETGVSPPACKETSIZE].ToString();
                result = true;
            }
            catch (Exception e)
            {
                result = false;
                SendERResponse(c_EXCEPTION, "", e.Message);
            }
            return result;

        }
        static string GetTextFromMessage(string message, int Idx)
        {
            try
            {
                string[] split = message.Split(new char[] { '<', '|', '>' }, StringSplitOptions.RemoveEmptyEntries);
                if (split.Length > Idx)
                    return split[Idx];
                else
                    return "";
            }
            catch (Exception exception)
            {
                SendERResponse(c_EXCEPTION, "", exception.Message);
                return "";
            }
        }
        static string GetIDFromMessage(string message)
        {
            try
            {
                string[] split = message.Split(new char[] { '<', '|', '>' }, StringSplitOptions.RemoveEmptyEntries);
                if (split.Length > 1)
                    return split[1];
                else
                    return "";
            }
            catch (Exception exception)
            {
                SendERResponse(c_EXCEPTION, "", exception.Message);
                return "";
            }
        }
        static string GetMessageParameter(string message)
        {
            string[] split = message.Split(new[] { '<', '|', '>' }, StringSplitOptions.RemoveEmptyEntries);
            return (split.Length > 1) ? split[1] : "";
        }

        static void CameraConnectionLost(Object sender, EventArgs e)
        {
            Camera cam = sender as Camera;
            if (cam != null)
            {
                DisconnectCam(cam.CameraInfo[CameraInfoKey.DeviceMacAddress]);
                SendOKResponse(commands[4], "", cam.CameraInfo[CameraInfoKey.DeviceMacAddress]);
            }
        }
        static bool ConnectCam(string ID, string IsSaveSettings, string FilePath, bool IsResetSettings)
        {
            bool result = false;
            try
            {
                DisconnectCam(ID);

            }
            catch (Exception e)
            {
                SendERResponse(c_EXCEPTION, ID, e.Message);
                result = false;
            }
            foreach (ICameraInfo camera in CameraFinder.Enumerate())
            {
                var lId = camera[CameraInfoKey.DeviceMacAddress];
                if (lId == ID)
                {
                    try
                    {
                        Camera cam = new Camera(camera);
                        cam.CameraOpened += Configuration.SoftwareTrigger;
                        cam.ConnectionLost += CameraConnectionLost;
                        Console.WriteLine(AddTimeStamp() + "-------------------------------------------------------------------connect " + ID);
                        SaveCAMSettingsOnOpen(cam, IsSaveSettings == "1", IsResetSettings);

                        cam.Open();
                        SendOKResponse(cc_MODEL, ID, cam.Parameters[PLCamera.DeviceFirmwareVersion].GetValue().ToString());

                        cam.Parameters[PLTransportLayer.HeartbeatTimeout].SetValue(10000);
                        cam.Parameters[PLCamera.GevSCPD].SetValue(4014);
                        cam.Parameters[PLCamera.ExposureTimeRaw].SetValue(25);
                        cam.Parameters[PLCamera.ExposureTimeAbs].SetValue(100000);
                        cam.Parameters[PLCamera.GevSCBWR].SetValue(10);
                        // string accessMode = cam.Parameters[PLStream.AccessMode].GetValue();
                        cam.Parameters[PLStream.AutoPacketSize].SetValue(true);
                        //cam.Parameters[PLCamera.GevSCPSPacketSize].SetValue(8912);
                        SendOKResponse(cc_ADJUSTGVSPPACKETSIZE, ID, cam.Parameters[PLCamera.GevSCPSPacketSize].GetValue().ToString());

                        // Set a handler for processing the images.
                        cam.StreamGrabber.ImageGrabbed += OnImageGrabbed;

                        // Start grabbing using the grab loop thread. This is done by setting the grabLoopType parameter
                        // to GrabLoop.ProvidedByStreamGrabber. The grab results are delivered to the image event handler OnImageGrabbed.
                        // The default grab strategy (GrabStrategy_OneByOne) is used.
                        cam.StreamGrabber.Start(GrabStrategy.OneByOne, GrabLoop.ProvidedByStreamGrabber);
                        if (!m_CamsConnected.ContainsKey(lId))
                            m_CamsConnected.Add(lId, cam);
                        GetStreamBytesPerSecond(ID);
                        result = true;

                    }
                    catch (Exception e)
                    {
                        SendERResponse(c_EXCEPTION, lId, e.Message);
                        result = false;
                    }
                }
            }
            return result;
        }
        static void GetStreamBytesPerSecond(string ID)
        {
            SendOKResponse(c_GETFEATUREVALUES, ID, c_StreamBytesPerSecond + "=10");
            //try
            // {
            //   SendOKResponse(c_GETFEATUREVALUES, ID, getFeatureList("<" + c_GETFEATUREVALUES + "|" + ID + "|" + c_StreamBytesPerSecond + ">"));
            // }
            //catch (Exception e)
            // {
            //   SendERResponse(c_EXCEPTION, ID, e.Message + c_GETFEATUREVALUES);
            // }
        }

        static void DisconnectCam(string ID)
        {
            if (!m_CamsConnected.ContainsKey(ID))
                return;
            try
            {
                m_CamsConnected[ID].Close();
            }
            finally
            {
                m_CamsConnected.Remove(ID);
                SendOKResponse("DISCONNECTCAM", ID, c_DISCONNECTED);
            }
        }
        static bool SendCameraSnapshot(string ID)
        {
            var result = false;
            try
            {
                var tmpMsg = new List<byte>();
                evt.Reset();
                m_CamsConnected[ID].ExecuteSoftwareTrigger();
                if (!m_CamsConnected[ID].WaitForFrameTriggerReady(2000, TimeoutHandling.Return))
                    return false;
                evt.WaitOne();
                if (!lastGrabSucceeded)
                    return false;
                string message = "FRAME";
                message = Concatenate(message, ID);
                message = Concatenate(message, lastBuffer.Length.ToString());
                message = Concatenate(message, lastGrabResult.Width.ToString());
                message = Concatenate(message, lastGrabResult.Height.ToString());
                message = "<" + message + ">";
                tmpMsg.AddRange(Encoding.ASCII.GetBytes(message));
                tmpMsg.AddRange(lastBuffer);
                stream.Write(tmpMsg.ToArray(), 0, tmpMsg.Count);
                result = lastGrabResult.GrabSucceeded;
                lastGrabResult.Dispose();
                /*if ((lastGrabresult.Width == 0) && (lastGrabresult.Height == 0) && CamsConnected[ID].Features.ContainsName("GVSPAdjustPacketSize")) {
                  CamsConnected[ID].Features["GVSPAdjustPacketSize"].RunCommand();
                  Console.WriteLine(AddTimeStamp() + "adjusting PacketSize for " + ID);
                }*/
            }
            catch (Exception e)
            {
                DisconnectCam(ID);
                SendERResponse("SNAPSHOT", ID, c_EXCEPTION + e.Message);
                return false;
            }
            return result;
        }

        // Example of an image event handler.
        static void OnImageGrabbed(Object sender, ImageGrabbedEventArgs e)
        {
            try
            {
                // The grab result is automatically disposed when the event call back returns.
                // The grab result can be cloned using IGrabResult.Clone if you want to keep a copy of it (not shown in this sample).
                IGrabResult grabResult = e.GrabResult;
                // Image grabbed successfully?
                lastGrabSucceeded = grabResult.GrabSucceeded;
                if (lastGrabSucceeded)
                {
                    lastBuffer = grabResult.PixelData as byte[];
                    lastGrabResult = grabResult.Clone();
                    // Console.WriteLine(AddTimeStamp() + "Grabbed length {0} Width {1} Height {2}", lastBuffer.Length, lastGrabResult.Width, lastGrabResult.Height);
                }
                else
                {
                    SendERResponse("SNAPSHOT", "", grabResult.ErrorCode + grabResult.ErrorDescription);
                }
                evt.Set();
            }
            catch (Exception exception)
            {
                SendERResponse(c_EXCEPTION, "", exception.Message);
            }

        }
        static void SendString(string messageToSend)
        {
            messageToSend = "<" + messageToSend + ">";
            byte[] msg = Encoding.ASCII.GetBytes(messageToSend);
            stream.Write(msg, 0, msg.Length);
        }
        static string Concatenate(string part1, string part2)
        {
            return part1 + '|' + part2;
        }

        static string AddTimeStamp()
        {
            DateTime now = DateTime.Now; // <-- Value is copied into local
            return now.ToString("yyyy.MM.dd hh:mm:ss:fff") + ": ";
        }
    }
}
