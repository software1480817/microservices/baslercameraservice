
pipeline {
    agent { label 'Windows_Jenkins_Agent' }

    options {
        // remove old builds
        buildDiscarder logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '10', daysToKeepStr: '', numToKeepStr: '10')
        // set timestamp in logs
        timestamps()
    }

    environment {
        DEBUG_MODE         = "false"
        SHOW_INFO          = "false"        
        PROJ_NAME          = 'BaslerCameraService'
        PROJ_EXE           = 'ConnectBaslerCameras.exe'
        PROJ_GROUP         = 'BaslerCameraServiceGroupVS2019.sln'
        PROJ_CONFIG        = 'Release'
        PROJ_PLATFORM      = 'Any CPU' //x86 x64
        OUTPUT_DIR         = '\\output'       
        ARTIFACTS_DIR      =  "\\Artifacts"
        INSTALL_SCRIPT     = ".\\scripts\\BaslerCameraService_install.iss"
        VersionNr          = getVersionNumber()
        VersionName        = getVersionNumberName()
        VersionType        = getVersionType()

        /* make adjustments in
            - stage "provide artifacts" : to provide your artifacts
            - post: 
                - success: archiveArtifacts
	            - failure: emailext: send email
         */
    }    
  
    stages {
        
        /////////////////////////////////////////////////////
        // Stage: Show Info
        /////////////////////////////////////////////////////
        stage("Set and show Info") {   
            steps{ 
                script {
                    if (getVersionNumber() == "") {
                        ShowInfo()
                        currentBuild.result = 'ABORTED'
                        error('Version Number not found!')
                    }
                    else {
                        currentBuild.displayName = VersionName
                    }
                }

                script {
                    if (SHOW_INFO == "true") {
                        ShowInfo()
                    }
                }                               
            }
        }

        /////////////////////////////////////////////////////
        // Stage: MSBuild
        /////////////////////////////////////////////////////
        stage("MSBuild") {
            environment {
                ARTIFACTS_DIR_REL       = ".${ARTIFACTS_DIR}"
            }                          
            steps{
                echo '************* MSBuild ************* '
                bat '"' + BUILD_HELPER_CSHARP + '" ' + WORKSPACE + ' ' + PROJ_GROUP + ' ' + PROJ_CONFIG + ' \"' + PROJ_PLATFORM + '\"'                     

                echo '************* Add Version info to the exe file *************'
                bat '"' + VERPATCH + '" ' + getOutputDir(WORKSPACE + "${OUTPUT_DIR}\\${PROJ_PLATFORM}\\${PROJ_CONFIG}") + '\\' + PROJ_EXE + ' ' + VersionNr
            }                                                                                               
        }    

        /////////////////////////////////////////////////////
        // Stage: Archive and provide artifacts in dir /Artifacts
        /////////////////////////////////////////////////////
        stage("provide artifacts") {
            environment {
                XCOPY_PARAMS            = ' /E/H/C/Y'
                ARTIFACTS_DIR_REL       = ".${ARTIFACTS_DIR}\\"
            }            
            steps{
                echo '************* Copy exe *************'    
                bat "xcopy " + getOutputDir(WORKSPACE + "${OUTPUT_DIR}\\${PROJ_PLATFORM}\\${PROJ_CONFIG}") + '\\*.exe' + ' ' + ARTIFACTS_DIR_REL + XCOPY_PARAMS 

                echo '************* Copy DLL *************'
                bat "xcopy " + getOutputDir(WORKSPACE + "${OUTPUT_DIR}\\${PROJ_PLATFORM}\\${PROJ_CONFIG}") + '\\*.dll' + ' ' + ARTIFACTS_DIR_REL + XCOPY_PARAMS 
            }                                                                                               
        }          

        /////////////////////////////////////////////////////
        // Stage: Installer
        /////////////////////////////////////////////////////
        stage("Installer") {
            environment {
                ARTIFACTS_DIR_REL       = ".${ARTIFACTS_DIR}"
                ARTIFACTS_DIR_INSTALL   = "${WORKSPACE}\\${ARTIFACTS_DIR}"
            }             
            steps{
                echo '************* Installer *************'
                script {
                    SetupFileName = getSetupFileName(PROJ_NAME)
                }
                bat '"' + ISCC + '" ' + '/DOpAppVersion=' + VersionNr + ' /DOpBuildDir=' + ARTIFACTS_DIR_INSTALL + ' /O' + ARTIFACTS_DIR_REL + ' /F' + SetupFileName + ' ' + INSTALL_SCRIPT
            }                                                                                               
        } 
    }

    post {
        always {
            
            echo '************* POST always *************'
        }   
        success {
            echo '************* POST success *************'
            archiveArtifacts followSymlinks: false, fingerprint: false, artifacts: ARTIFACTS_DIR + '\\*.exe'
            archiveArtifacts followSymlinks: false, fingerprint: false, artifacts: ARTIFACTS_DIR + '\\*.dll'
        }        
        failure {
            echo '************* POST failure *************'
            script {
                if (DEBUG_MODE == "false") {
                    emailext attachLog: true, 
                            body: '$DEFAULT_CONTENT', 
                            recipientProviders: [developers(), requestor()], 
                            subject: '$DEFAULT_SUBJECT' , 
                            recipients: emailextrecipients([buildUser(), developers(), requestor()])
                            //to: '$BUILD_RECIPIENTS' // Jenkins admins
                            // see https://www.jenkins.io/doc/pipeline/steps/email-ext/
                }       
            }          
        }

        cleanup {
            echo '************* POST cleanup *************'
            script {
                try { // cleaning Workspace. otherwise can be used asynch cleanWs
                    if (DEBUG_MODE == "false") {
                        deleteDir()
                    }
                }
                catch(Exception e) {
                    echo 'Error: Cannot Clean output: ' + e.toString()
                }   
            }             
        }             
    } 
}


def getVersionNumberName() {
    return 'V' + getVersionNumber()
}

def getVersionNumber() {


    // expected TAG V1.2.3
    // Result: 1.2.3
    /* testet variations
        " " | "V1.2.3." | "Version1.2.3." | "V1.2.3" | "1.2.3." | "1.2.3"
    */

    vVers = getCommandOutput("git describe --tags --abbrev=0")

    // remove all spaces
    vVers = vVers.replace(' ', '');
    if (vVers.length() == 0) {
        vResult = ""
    }
    else { // find from TAG name the first number
        /* debugging 
        for(int i in 0..vVers.length() -1) { 
            println "[" + i + "]: " + vVers[i]
        }   
        */
        for(int i in 0..vVers.length() -1) { 
            if (vVers[i].isNumber()) {
                //println "i: " + i + " | " + vVers[i]
                vResult = vVers.substring(i, vVers.length())
                break
            }
        }      

        if ( ! vResult[vResult.length()-1].isNumber()) { // 1.2.3. ends with . => remove it
            vResult = vResult.substring(0, vResult.length()-1)
        }  

         vResult = vResult + '.' + currentBuild.number      
    }

    return vResult // returning e.g. 1.2.3.267
}

def getSetupFileName(Prefix) {
    return 'Setup_' + Prefix + '_' + VersionName // e.g. Setup_Tool_V3.15.0.exe
}

def getVersionType() {
    // 3: VersionType: 1: Release, 2: Testversion, 3: Nightlybuild, 4: Hotfix-Testversion, 5: Featurebuild
    // only Release supportet for Tools
    return '1'
}

def getCommandOutput(cmd) {
    script{
        echo "getCommandOutput: " + cmd
        if (isUnix()){
            return sh(returnStdout:true , script: cmd).trim()
        } else{
            stdout = bat(returnStdout:true , script: cmd).trim()
            result = stdout.readLines().drop(1).join(" ")   
            echo "... " + result    
            return result
        } 
    }
} 

def getOutputDir(dir) {
    script{
        echo 'getOutputDir: ' + dir + ' => ' + dir.replaceAll(" ","")
        return dir.replaceAll(" ","")
    }
} 

def ShowInfo() {
    echo '************* Show Info *************'              
    echo 'JENKINS_HOME: ' + JENKINS_HOME + ' | BUILD_HELPER_DELPHI: ' + BUILD_HELPER_DELPHI + ' | WORKSPACE: ' + WORKSPACE
    echo 'PROJ_GROUP: ' + PROJ_GROUP + ' | PROJ_NAME: ' + PROJ_NAME + ' | PROJ_CONFIG: ' + PROJ_CONFIG + ' | PROJ_PLATFORM: ' + PROJ_PLATFORM 
    echo 'JOB_NAME: ' + JOB_NAME + ' | JOB_URL: ' + JOB_URL + ' | BUILD_NUMBER: ' + BUILD_NUMBER + ' | BUILD_TAG: ' + BUILD_TAG + ' | GIT_COMMIT: ' + GIT_COMMIT
    echo 'OPTOOLS: ' + OPTOOLS + ' | CHECKEUREKALOGUSES: ' + CHECKEUREKALOGUSES     
    echo 'VersionNr: ' + VersionNr +  ' | displayName: ' + currentBuild.displayName 
}